This small script will prompt the user for a system path (full path).

Then it will create, in the provided folder path, a file (out.txt) that will 
contain a list of every files and folders that are currently in that folder.


If "out.txt" already exist, the program stops and raise an exception.
