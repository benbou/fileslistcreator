import os


print("Please enter a system path:")
os.chdir(input())

files = os.listdir()
files.sort()


if "out.txt" not in files:
    with open("out.txt", "w", encoding="utf-8") as f:
        f.write("\n".join(files))
else:
    raise FileExistsError


print("Job done successfully.")
